<?php
	
	if(isset($_POST["enviar"])){
		$nombre=$_POST["nombre"];
		$email=$_POST["email"];
		$asunto=$_POST["asunto"];
		$telefono=$_POST["telefono"];
		$mensaje=$_POST["mensaje"];

		$error_vacio=false;
		$error_email=false;
		$vacio_nombre=false;
		$vacio_asunto=false;
		$vacio_mensaje=false;
		$vacio_email=false;
		if(empty($nombre)){
			$vacio_nombre=true;
			$error_vacio=true;
		}
		if(empty($asunto)){
			$vacio_asunto=true;
			$error_vacio=true;
		}
		if(empty($mensaje)){
			$vacio_mensaje=true;
			$error_vacio=true;
		}
		if(empty($email)){
			$vacio_email=true;
			$error_vacio=true;
		}
		if($error_vacio==true){
			echo "<span class='error-form'><i class='far fa-frown'></i> Rellena los campos obligatorios del formulario</span>";
			$error_vacio=true;
		}else{
			if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
				echo "<span class='error-form'><i class='far fa-frown'></i> Introduce una dirección de correo válida</span>";
				$error_email=true;
			}else{
				$to="bertoceballos@hotmail.com";
				$subject=$asunto;
				//$header="From:".$email."\r\n";
				$body="De:".$nombre." Telef:".$telefono."\n".$email."\n".$mensaje;

				require_once 'swiftmailer/vendor/autoload.php';

				$transport = (new Swift_SmtpTransport('217.116.0.228', 25))
             		->setUsername('alberto@albertoceballos.es')
            		->setPassword('Gallagher83');
 
				//Creamos el mailer pasándole el transport con la configuración
				$mailer = (new Swift_Mailer($transport));
 
				//Creamos el mensaje
				$message = (new Swift_Message("albertoceballos.es: ".$subject))
            		->setFrom([$email => $nombre])
            		->setTo(['bertoceballos@gmail.com'=>'Alberto Ceballos'])
            		->setBody($body);
 
				//Enviamos
				$result = $mailer->send($message);
				if (!$result) {
					echo "<span class='error-form'><i class='far fa-frown'></i>Hubo algún error al enviar el correo<i class='fas fa-exclamation-triangle'></i></span>";
				}else{
					echo "<span class='success-form'><i class='far fa-smile'></i>Mensaje enviado con éxito. Gracias<i class='fas fa-envelope'></i></span>";
				}
						
			}
		}
	}else{
		echo "Hubo algún error";
	}
?>
<script type="text/javascript">
	$("[id^=form-]").removeClass("is-invalid");
	var error_email="<?php echo $error_email; ?>";
	var vacio_nombre="<?php echo $vacio_nombre; ?>";
	var vacio_asunto="<?php echo $vacio_asunto; ?>";
	var vacio_email="<?php echo $vacio_email; ?>";
	var vacio_mensaje="<?php echo $vacio_mensaje; ?>";
	if(vacio_nombre==true){
		$("#form-nombre").addClass("is-invalid");
	}
	if(vacio_asunto==true){
		$("#form-asunto").addClass("is-invalid");
	}
	if(vacio_mensaje==true){
		$("#form-mensaje").addClass("is-invalid");
	}
	if(vacio_email==true || error_email==true){
		$("#form-email").addClass("is-invalid");
	}
</script>