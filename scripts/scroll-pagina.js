/*Código para el efecto de scroll sueave en la página al hacer click en el menú lateral*/
$("document").ready(function(){
 $('#barnav').click(function(e){
    if ($(e.target).is('a.enlaces-menu')) {
        if (location.pathname.replace(/^\//, '') == e.target.pathname.replace(/^\//, '') && location.hostname == e.target.hostname) {
            var target = $(e.target.hash);
            target = target.length ? target : $('[name=' + e.target.hash.slice(1) + ']');
            if (target.length) {
                var gap = 0;
                $('html,body').animate({
                    scrollTop: target.offset().top - gap
                }, 1000);
            }      
        }
        return false;
    }
  });
});