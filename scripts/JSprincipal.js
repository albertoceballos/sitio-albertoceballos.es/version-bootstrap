//Iniciar la libreria wow que carga los animate al hacer scroll, e iniciar opciones

    wow = new WOW({
        animateClass: 'animated',
        offset: 100,
        callback: function(box) {
            //console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
    });
    wow.init();

    //Oculto el contenido principal y la barra de menu de arriba con opacity 0 hasta que no se hayan cargado todos los elementos de la ventana, así solo se verá la precarga. No pongo un hide() porque sino la librería WOW me cargaría todos los elementos al principio en Chrome, lo interpreta mal.     
    $("document").ready(function() {
        $("#contenido").css('opacity', '0.0');
        $(".menu-arriba").css('opacity','0.0');
    });
    $(window).on("load", function() {
        //FadeOut para la precarga y cambiar el background y el opacity a 1.0 al contenido principal + efecto Fade para barra lateral
        $("#loading").fadeOut("slow", function() {
            $("body").css('background-image','linear-gradient(#0CABA6,#08726E');
            $(".menu-arriba").css('opacity','1.0');
            $("#contenido").animate({ opacity: "1.0" }, 600, function() {
                $("#barnav").animate({ left: "10px" }, 1200, "easeOutSine");
            });
        });

        //Script para la función de TypeIT del efecto de tecleear
        var instance = new TypeIt('#div-type', {
            speed: 150,
        }).type('Diseñador/').break().type('Desarrollador Web').pause('1500').delete().type('Front-End/Back-End Developer')
        .delete(28).type('Full-Stack Developer');
        //Efectos al pasar ratón por las barras de progreso
        $(".bloque-barra").hover(function() {
            $(this).children(".progress").toggleClass("progress-hover");
            $(".porcentaje").css('text-shadow', 'none');
        });
        //Activar tooltips y efectos de iconos de aptitudes
        $(".aptitud").hover(function() {
            var i = $(".aptitud").index(this) + 1;
            $("#aptitud_text" + i).fadeIn(500);
            $(".aptitud" + i).addClass("aptitud" + i + "__hover");
        }, function() {
            var i = $(".aptitud").index(this) + 1;
            $("#aptitud_text" + i).fadeOut(500);
            $(".aptitud" + i).removeClass("aptitud" + i + "__hover");
        });
        /*script para ocultar/mostrar el "back to top"*/
        $("#back-top").hide();
        $(window).scroll(function(){
            if ($(this).scrollTop() > 150) {
                $("#back-top").fadeIn();
            } else {
                $("#back-top").fadeOut();
            }
        });
        $("#back-top").click(function() {
            $("html").animate({scrollTop: "0"}, 1000);
        });
        /*Función para opciones del mapa de Google maps
        function myMap() {
            var latitud = { lat: 43.461653, lng: -3.818299 };
            var mapProp = {
                center: latitud,
                mapTypeControl: false,
                zoom: 14
            };
            var map = new google.maps.Map(document.getElementById("mapa"), mapProp);
            var marker = new google.maps.Marker({
                map: map,
                position: latitud
            });
        }
        //Iniciar la función del mapa de Google
        myMap();*/

        //Función para abrir el menú en el icono de la barra superior
        $(".icono-menu").click(function() {
            if (!$(this).hasClass("icono-menu-activo")) {
                $(this).addClass("icono-menu-activo");
                $(".barralateral").animate({ left: "0px" }, 500);
            } else {
                $(this).removeClass("icono-menu-activo");
                $(".barralateral").animate({ left: "-250px" }, 500);
            }
        });
        /*función para que el menú no se quede a la izda en caso de redimensionar la ventana y cerrar el menú, 
        porque con media queries no lo hace(no sé por qué)*/
        $(window).on('resize', function() {
            var win = $(this);
            if (win.width() >= 992) {
                $(".barralateral").css({ left: "0px" });
            } else {
                if (!$(".icono-menu").hasClass("icono-menu-activo")) {
                    $(".barralateral").css({ left: "-250px" });
                }
            }
            if(win.width()<576){
                $(".barralateral").css({left: "0px"});
            }
        });
        //Ocultar menú lateral al hacer click en los enlaces (sólo cuando esté la barra superior)
        $(".enlaces-menu").click(function() {
            if (($(window).width()) < 992 && ($(window).width())>576){
                $(".barralateral").css({ left: "-250px" }, 500);
                $(".icono-menu").removeClass("icono-menu-activo");
            }
        });


        //Envio del formulario de contacto y respuesta mediante Ajax
        $("#formContacto").submit(function(evento) {
            evento.preventDefault();
            var nombre = $("#form-nombre").val();
            var email = $("#form-email").val();
            var asunto = $("#form-asunto").val();
            var telefono = $("#form-tel").val();
            var mensaje = $("#form-mensaje").val();
            var enviar = $("#form-enviar").val();
            $.ajax({
                url: "contacto.php",
                type: "POST",
                data: ({
                    nombre: nombre,
                    email: email,
                    asunto: asunto,
                    telefono: telefono,
                    mensaje: mensaje,
                    enviar: enviar
                }),
                beforeSend: function() {
                    $("#respuestaForm").html("<img src='imagenes/ajax-loader.gif' />");
                },
                success: function(datos) {
                    $("#respuestaForm").html(datos);
                }
            });
        });
    });